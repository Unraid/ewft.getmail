<?PHP
# Setup page variables
$appname="ewft.getmail";
$displayname="GetMail";
$appexecutable="usr/sbin/minidlnad";
$appconfigfile="settings.json";
$plglogfile="/var/log/minidlnad/{$appname}.log";
$arrayState=trim(shell_exec( "grep fsState /var/local/emhttp/var.ini | sed -n 's!fsState=\"\(.*\)\"!\\1!p'" ));
$ver60check=trim(shell_exec( "grep version /etc/unraid-version | sed -n 's!version=\"\(.*\)\"!\\1!p'" ));


# Sets the loader background to match the theme based on unRaid version sicne 6 has a dark theme
$machine_type = trim(shell_exec( "uname -m" ));
if ($machine_type == "x86_64") {
	$loaderbgcolor = "html";
	$prefix = "";
	if (substr($ver60check, 0, 3) === "6.0") {
		$prefix = "/usr/local/emhttp";
	}
} else {
	$loaderbgcolor = ".Container";
	$prefix = "/usr/local/emhttp";
}
?>

<!-- ========================================================================= -->
<!-- Page load animation and text -->
<!-- ========================================================================= -->

<!-- # Add jquery library, show loader, fade out when loaded -->
<script type="text/javascript">
	if(typeof jQuery == 'undefined'){
		var oScriptElem = document.createElement("script");
		oScriptElem.type = "text/javascript";
		oScriptElem.src = "/plugins/<?=$appname;?>/scripts/jquery.min.js";
		document.head.insertBefore(oScriptElem, document.getElementsByTagName("script")[0])
    }
</script>
<div id="loading">
	<img id="loading-image" src='/plugins/<?=$appname;?>/icons/loader.gif' alt="Loading..." />
	<p id="loading-text">LOADING PLUGIN</p>
</div>
<script language="javascript" type="text/javascript">
	$("#loading").css("background-color",$("<?=$loaderbgcolor;?>").css("background-color"));
	$("#loading").show();
</script>
<script language="javascript" type="text/javascript">
	$(window).load(function() {$("#loading").fadeOut("slow");});
</script>

<!-- ========================================================================= -->
<!-- Load current config file and check if program is installed already -->
<!-- ========================================================================= -->

<?PHP

# This will clean any ^M characters caused by windows from the config file before use
if (file_exists("/boot/config/plugins/{$appname}/{$appname}.cfg"))
	shell_exec("sed -i 's!\r!!g' \"/boot/config/plugins/{$appname}/{$appname}.cfg\"");
if (file_exists("/usr/local/emhttp/plugins/{$appname}/scripts/rc.{$appname}"))
	shell_exec("sed -i 's!\r!!g' \"/usr/local/emhttp/plugins/{$appname}/scripts/rc.{$appname}\"");

# Check existence of files and make startfile if missing
$app_cfg = parse_ini_file( "/boot/config/plugins/{$appname}/{$appname}.cfg" );
$app_installed = file_exists("/{$appexecutable}" ) ? "yes" : "no";



# =========================================================================
## Collect local variables from config files and verify data as best as possible
# =========================================================================



# Set readonly status if array is offline
if ($arrayState == "Started") {
	$app_readonly = "";
	$app_disabled = "";
} else {
	$app_readonly = 'readonly="readonly"';
	$app_disabled = 'disabled="disabled"';
}

# Plugin Current Version Variable
if (file_exists("/boot/config/plugins/{$appname}.plg")) {
	$app_plgver = trim(shell_exec ( "grep 'ENTITY version' /boot/config/plugins/{$appname}.plg | sed -n 's!.*\s\"\(.*\)\".*!\\1!p'" ));
	if ($app_plgver == "") {
		$app_plgver = "Unknown plugin version";
	}
} else {
	$app_plgver = "(Plugin File Missing)";
}



# Service Status Variable
if (isset($app_cfg['SERVICE']) && ($app_cfg['SERVICE'] == "enable" || $app_cfg['SERVICE'] == "disable"))
	$app_service = $app_cfg['SERVICE'];
else
	$app_service = "disable";



# =========================================================================
## Check is program is installed and running to get extra information
# =========================================================================

# Get current installed version of the program
if ($arrayState=="Started") {
	$app_curversion = trim(shell_exec ( "/usr/local/emhttp/plugins/{$appname}/scripts/rc.{$appname} currentversion" ));
	if ($app_curversion == "")
		$app_curversion = "UNKNOWN";
}

if ($app_installed=="yes" && $arrayState=="Started") {
	$app_running = trim(shell_exec( "[ -f /proc/`cat /var/run/minidlna/minidlna.pid 2> /dev/null`/exe ] && echo 'yes' || echo 'no' 2> /dev/null" ));
	if ($app_running == "yes")
		$app_updatestatus = "Running";
	else
		$app_updatestatus = "Stopped";

}

echo "<script>document.getElementById('loading-text').innerHTML = \"DONE\";</script>";
?>

<!-- ========================================================================= -->
<!-- Create the HTML code used to display the settings GUI -->
<!-- ========================================================================= -->


<script type="text/javascript">

function openLog() {
	var machine_type = "<?=$machine_type;?>";
	var title="<?=$appname;?> Activity Log";
	if (machine_type == "x86_64") {
		<?if (substr($ver60check, 0, 3) === "6.0"):?>
			var url="/usr/bin/tail -n 42 -f <?=$plglogfile;?>";
			openWindow( url, title,600,900);
		<?else:?>
			var url="<?=$prefix;?>/plugins/<?=$appname;?>/scripts/tail_log&arg1=<?=$appname;?>.log";
			openWindow( url, title,600,900);
		<?endif;?>
	} else {
		var url="/logging.htm?title=" + title + "&cmd=/usr/bin/tail -n 42 -f <?=$plglogfile;?>&forkCmd=Start";
		openWindow( url, title.replace(/ /g, "_"),600,900);
	}
}

</script>

