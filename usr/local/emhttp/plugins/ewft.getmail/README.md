**GetMail**

GetMail is a flexible, extensible mail retrieval system with support for POP3, IMAP4, SSL variants of both, maildirs, mboxrd files, external MDAs, arbitrary message filtering, single-user and domain-mailboxes, and many other useful features.

https://github.com/ewft/getmail6
